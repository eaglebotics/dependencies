# Dependencies for NBPSEaglebotics/robot-steamworks

This repository contains all dependencies for the robot-steamworks repository (as of 2/15/17).

It is recommeneded that you clone this onto your computer and update your build paths to link to it. Then, when you either sync in Github Desktop or use "git pull" in the terminal, you will have updated your dependencies.
